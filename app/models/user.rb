class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

   include Amistad::FriendModel

   has_many :posts, dependent: :destroy
   has_many :projects, dependent: :destroy
   has_many :tasks, through: :projects
end
