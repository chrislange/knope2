class WelcomeController < ApplicationController

	def index

		@user = current_user
		if user_signed_in?
			
			@friends = current_user.friends
			
			
			@announcements = @user.posts.where({post_type: 1}) + Post.where(:user_id => current_user.friends, post_type: 1)
			@announcements.sort! { |a, b| b.created_at <=> a.created_at }  
			@compliments = @user.posts.where({post_type: 2}) + Post.where(:user_id => current_user.friends, post_type: 2)
			@compliments.sort! { |a, b| b.created_at <=> a.created_at } 
			

			@users = User.all
			

		end
	end
end
