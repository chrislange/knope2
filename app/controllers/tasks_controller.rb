class TasksController < ApplicationController

	def create 
		@task = current_user.tasks.build(task_params)

		if @task.save
      		flash[:success] ="Added!"
     		redirect_to root_path
     	end

	end

	def destroy
		Task.find(params[:id]).destroy
		redirect_to root_path
	end

	def toggle_completed
		@task = Task.find(params[:id])
		@task.completed = !@task.completed
		@task.save
		redirect_to :back
	end

	 private
    def task_params
      params.require(:task).permit(:project_id, :content, :completed)
    end


end