class ProjectsController < ApplicationController


	def create
		@user = current_user
		@project = @user.projects.create(project_params)
		if @project.save
			redirect_to root_path
		else
			redirect_to user_path(current_user)
		end
	end

	def destroy
		Project.find(params[:id]).destroy
		redirect_to root_path
	end

	private 
	def project_params
		params.require(:project).permit(:title, :completed)
	end
end