class PostsController < ApplicationController

	def index
		@posts = Post.all
	end
	def new
		@user = current_user
		@post = @user.posts.create
	end

	def create
		@user = current_user
		@post = @user.posts.create(post_params)
		if @post.save
			redirect_to root_path
		end
	end

	def show
		@user = current_user
		@post = Post.find(params[:id])
		@author = User.where({id: @post.user_id})
		
	end

	def update

	end
	def destroy
		Post.find(params[:id]).destroy
		redirect_to root_path
	end

	private 
	def post_params
		params.require(:post).permit(:title, :content, :post_type)
	end
end