class UsersController < ApplicationController

	def index
		@users = User.all
	end

	def show
		@user = User.find(params[:id])
		@post = @user.posts.new
		@posts = @user.posts	
		@users = User.all
		
		@friends = current_user.friends
	end

end