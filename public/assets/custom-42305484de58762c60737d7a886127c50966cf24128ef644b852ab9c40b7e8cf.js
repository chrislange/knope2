$(document).ready(main)

var main = function(){
	$("#comp-button").click(function(){
		
		var result = "Anne you " + $('#comp-val1').val() + ", " + $('#comp-val2').val() + ", " + $('#comp-val3').val() + ".";
		$("#comp-result").val(result);
	
	});
	
	$('[data-toggle="popover"]').popover()

   $("#popoverFriends").popover({
        html : true, 
        
        content: function() {
          return $('#popoverFriendsHiddenContent').html();
        },
        title: function() {
          return $('#popoverFriendsHiddenTitle').html();
        }
    });

    

    $(".error").fadeOut(5000); //Add a fade out effect that will last for 2000 millisecond
    $(".alert").fadeOut(5000);
    $(".notice").fadeOut(5000);
    $(".success").fadeOut(5000);
    $(".info").fadeOut(5000);
      
};
