$(document).ready(main)

var main = function(){
	$("#comp-button").click(function(){
		
		var result = "Anne you " + $('#comp-val1').val() + ", " + $('#comp-val2').val() + ", " + $('#comp-val3').val() + ".";
		$("#comp-result").val(result);
	
	});
	
	$('[data-toggle="popover"]').popover()

   $("#popoverFriends").popover({
        html : true, 
        
        content: function() {
          return $('#popoverFriendsHiddenContent').html();
        },
        title: function() {
          return $('#popoverFriendsHiddenTitle').html();
        }
    });
};
