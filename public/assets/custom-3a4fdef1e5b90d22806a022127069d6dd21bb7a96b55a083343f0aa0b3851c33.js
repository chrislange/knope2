$(document).ready(main)

var main = function(){
	$("#comp-button").click(function(){
		
		var result = "Anne you " + $('#comp-val1').val() + ", " + $('#comp-val2').val() + ", " + $('#comp-val3').val() + ".";
		$("#comp-result").val(result);
	
	});
	
	$('[data-toggle="popover"]').popover()

   $("#popoverFriends").popover({
        html : true, 
        
        content: function() {
          return $('#popoverFriendsHiddenContent').html();
        },
        title: function() {
          return $('#popoverFriendsHiddenTitle').html();
        }
    });

   $(".checkbutton").click(function() {
      $(this).parents("li span").css("text-decoration", "line-through");
    });

   $(function(){
    var list = [
      "Make the best grilled cheese sandwich ever", 
      "Sand down toenails",
      "Watch all 8 Harry Potter movies"
      ],
      r = Math.floor(Math.random() * list.length);
      $('#todofield').prop('placeholder',list[r]);
}   );

    

    $(".error").fadeOut(5000); //Add a fade out effect that will last for 2000 millisecond
    $(".alert").fadeOut(5000);
    $(".notice").fadeOut(5000);
    $(".success").fadeOut(5000);
    $(".info").fadeOut(5000);
      
};
